#include <linux/unistd.h>
#include <linux/errno.h>
#include <linux/kernel.h>
#include <linux/sched.h>

asmlinkage int sys_newsyscall(int n) {
	printk("Hello\n");
	return n*n;
}
