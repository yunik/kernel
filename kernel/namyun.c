#include <linux/unistd.h>
#include <linux/errno.h>
#include <linux/kernel.h>
#include <linux/sched.h>

asmlinkage int sys_namyun(int x, int y) {
	int i=0;
	long long result=1;
	for(; i<y; i++) {
		result=result*x;
	}
	return result;
}
