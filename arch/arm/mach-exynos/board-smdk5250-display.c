/* linux/arch/arm/mach-exynos/board-smdk5250-display.c
 *
 * Copyright (c) 2012 Samsung Electronics Co., Ltd.
 *		http://www.samsung.com
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
*/

#include <linux/gpio.h>
#include <linux/pwm_backlight.h>
#include <linux/fb.h>
#include <linux/delay.h>
#include <linux/clk.h>

#include <video/platform_lcd.h>
#include <video/s5p-dp.h>

#include <plat/cpu.h>
#include <plat/clock.h>
#include <plat/devs.h>
#include <plat/fb.h>
#include <plat/fb-s5p.h>
#include <plat/fb-core.h>
#include <plat/regs-fb-v4.h>
#include <plat/dp.h>
#include <plat/pd.h>
#include <plat/backlight.h>

#include <mach/map.h>
#include <mach/dev.h>

#ifdef CONFIG_FB_MIPI_DSIM
#include <plat/dsim.h>
#include <plat/mipi_dsi.h>
#endif

#include "board-smdk5250.h"

static void mipi_lcd_set_backlight(unsigned int power) {
	gpio_request(EXYNOS5_GPB2(0), "LCD Backlight EN");
#if defined(CONFIG_BACKLIGHT_PWM)
	if(power) {
		s3c_gpio_cfgpin(EXYNOS5_GPB2(0), S3C_GPIO_SFN(0x2));
	} else {
		s3c_gpio_cfgpin(EXYNOS5_GPB2(0), S3C_GPIO_INPUT);
	}
#else 
	gpio_direction_output(EXYNOS5_GPB2(0), power ? 1 : 0);
#endif
	gpio_free(EXYNOS5_GPB2(0));
}

#if defined(CONFIG_LCD_MIPI_TC358764)
static void mipi_lcd_set_power(struct plat_lcd_data *pd,
				unsigned int power)
{
	printk(KERN_INFO "[%s:%d] %s() , power = %s\n",
			__FILE__, __LINE__, __func__, 
			power ? "ON" : "OFF");
	
	gpio_request(EXYNOS5_GPE0(4), "LCD Standby");
	gpio_request(EXYNOS5_GPE0(3), "LCD Reset");
	gpio_request(EXYNOS5_GPF1(2), "LCD Gate Voltage");
	gpio_request(EXYNOS5_GPF1(0), "MIPI2LVDS Converter Reset");
	
	gpio_direction_output(EXYNOS5_GPE0(3), 0);
	gpio_direction_output(EXYNOS5_GPE0(4), 0);
	gpio_direction_output(EXYNOS5_GPF1(0), 0);
	gpio_direction_output(EXYNOS5_GPF1(2), 0);
	mdelay(10);

	if(power) {
		gpio_direction_output(EXYNOS5_GPE0(4), 1);
		gpio_direction_output(EXYNOS5_GPE0(3), 1);
		mdelay(10);
		gpio_direction_output(EXYNOS5_GPF1(2), 1);
		mdelay(20);
		gpio_direction_output(EXYNOS5_GPF1(0), 1);
		mdelay(50);
		mipi_lcd_set_backlight(power);
	} else {
		mipi_lcd_set_backlight(power);
		mdelay(20);
		gpio_direction_output(EXYNOS5_GPE0(3), 0);
		gpio_direction_output(EXYNOS5_GPE0(4), 0);
		gpio_direction_output(EXYNOS5_GPF1(0), 0);
		gpio_direction_output(EXYNOS5_GPF1(2), 0);
	}

	gpio_free(EXYNOS5_GPE0(4));
	gpio_free(EXYNOS5_GPE0(3));
	gpio_free(EXYNOS5_GPF1(2));
	gpio_free(EXYNOS5_GPF1(0));
}
#endif

static struct plat_lcd_data smdk5250_mipi_lcd_data = {
	.set_power	= mipi_lcd_set_power,
};

static struct platform_device smdk5250_mipi_lcd = {
	.name			= "platform-lcd",
	.dev.platform_data	= &smdk5250_mipi_lcd_data,
};

static struct s3c_fb_pd_win smdk5250_fb_win0 = {
	.win_mode = {
		.left_margin	= 4,
		.right_margin	= 4,
		.upper_margin	= 4,
		.lower_margin	= 4,
		.hsync_len	= 4,
		.vsync_len	= 4,
		.xres		= 1024,
		.yres		= 600,
	},
	.virtual_x		= 1024,
	.virtual_y		= 640 * 2,
	.width			= 223,
	.height			= 125,
	.max_bpp		= 32,
	.default_bpp		= 24,
};

static struct s3c_fb_pd_win smdk5250_fb_win1 = {
	.win_mode = {
		.left_margin	= 4,
		.right_margin	= 4,
		.upper_margin	= 4,
		.lower_margin	= 4,
		.hsync_len	= 4,
		.vsync_len	= 4,
		.xres		= 1024,
		.yres		= 600,
	},
	.virtual_x		= 1024,
	.virtual_y		= 640 * 2,
	.width			= 223,
	.height			= 125,
	.max_bpp		= 32,
	.default_bpp		= 24,
};

static struct s3c_fb_pd_win smdk5250_fb_win2 = {
	.win_mode = {
		.left_margin	= 4,
		.right_margin	= 4,
		.upper_margin	= 4,
		.lower_margin	= 4,
		.hsync_len	= 4,
		.vsync_len	= 4,
		.xres		= 1024,
		.yres		= 600,
	},
	.virtual_x		= 1024,
	.virtual_y		= 640 * 3,
	.width			= 223,
	.height			= 125,
	.max_bpp		= 32,
	.default_bpp		= 24,
};

static void exynos_fimd_gpio_setup_24bpp(void)
{
	unsigned int reg = 0;
	/*
	 * Set DISP1BLK_CFG register for Display path selection
	 *
	 * FIMD of DISP1_BLK Bypass selection : DISP1BLK_CFG[15]
	 * ---------------------
	 *  0 | MIE/MDNIE
	 *  1 | FIMD : selected
	 */
	reg = __raw_readl(S3C_VA_SYS + 0x0214);
	reg &= ~(1 << 15);	/* To save other reset values */
	reg |= (1 << 15);
	__raw_writel(reg, S3C_VA_SYS + 0x0214);
}

static struct s3c_fb_platdata smdk5250_lcd1_pdata __initdata = {
#if defined(CONFIG_LCD_MIPI_S6E8AB0) || defined(CONFIG_LCD_MIPI_TC358764) || \
	defined(CONFIG_S5P_DP)
	.win[0]		= &smdk5250_fb_win0,
	.win[1]		= &smdk5250_fb_win1,
	.win[2]		= &smdk5250_fb_win2,
#endif
	.default_win	= 2,
	.vidcon0	= VIDCON0_VIDOUT_RGB | VIDCON0_PNRMODE_RGB,
#if defined(CONFIG_LCD_MIPI_S6E8AB0) || defined(CONFIG_LCD_MIPI_TC358764)
	.vidcon1	= VIDCON1_INV_VCLK,
#endif
	.setup_gpio	= exynos_fimd_gpio_setup_24bpp,
};

#ifdef CONFIG_FB_MIPI_DSIM
#if defined(CONFIG_LCD_MIPI_TC358764)
static struct mipi_dsim_config dsim_info = {
	.e_interface	= DSIM_VIDEO,
	.e_pixel_format	= DSIM_24BPP_888,
	/* main frame fifo auto flush at VSYNC pulse */
	.auto_flush	= false,
	.eot_disable	= false,
	.auto_vertical_cnt = true,
	.hse = false,
	.hfp = false,
	.hbp = false,
	.hsa = false,

	.e_no_data_lane	= DSIM_DATA_LANE_4,
	.e_byte_clk	= DSIM_PLL_OUT_DIV8,
	.e_burst_mode	= DSIM_BURST,

	.p = 3,
	.m = 80,
	.s = 1,

	/* D-PHY PLL stable time spec :min = 200usec ~ max 400usec */
	.pll_stable_time = 500,

	.esc_clk = 10 * 1000000,		/* escape clk : 10MHz */

	/* stop state holding counter after bta change count 0 ~ 0xfff */
	.stop_holding_cnt	= 0x0f,
	.bta_timeout		= 0xff,		/* bta timeout 0 ~ 0xff */
	.rx_timeout		= 0xffff,	/* lp rx timeout 0 ~ 0xffff */

	.dsim_ddi_pd = &tc358764_mipi_lcd_driver,
};

static struct mipi_dsim_lcd_config dsim_lcd_info = {
	.rgb_timing.left_margin		= 0x4,
	.rgb_timing.right_margin	= 0x4,
	.rgb_timing.upper_margin	= 0x4,
	.rgb_timing.lower_margin	= 0x4,
	.rgb_timing.hsync_len		= 0x4,
	.rgb_timing.vsync_len		= 0x4,
	.cpu_timing.cs_setup		= 0,
	.cpu_timing.wr_setup		= 1,
	.cpu_timing.wr_act		= 0,
	.cpu_timing.wr_hold		= 0,
	.lcd_size.width			= 1024,
	.lcd_size.height		= 600,
};
#endif

static struct s5p_platform_mipi_dsim dsim_platform_data = {
	.clk_name		= "dsim0",
	.dsim_config		= &dsim_info,
	.dsim_lcd_config	= &dsim_lcd_info,

	.part_reset		= s5p_dsim_part_reset,
	.init_d_phy		= s5p_dsim_init_d_phy,
	.get_fb_frame_done	= NULL,
	.trigger		= NULL,

	/*
	 * the stable time of needing to write data on SFR
	 * when the mipi mode becomes LP mode.
	 */
	.delay_for_stabilization = 600,
};
#endif

static struct platform_device *smdk5250_display_devices[] __initdata = {
#ifdef CONFIG_FB_MIPI_DSIM
	&smdk5250_mipi_lcd,
	&s5p_device_mipi_dsim,
#endif
	&s5p_device_fimd1,
};

/* LCD Backlight data */
static struct samsung_bl_gpio_info smdk5250_bl_gpio_info = {
	.no = EXYNOS5_GPB2(0),
	.func = S3C_GPIO_SFN(2),
};

static struct platform_pwm_backlight_data smdk5250_bl_data = {
	.pwm_id = 0,
	.pwm_period_ns = 20000,
};

void __init exynos5_smdk5250_display_init(void)
{
#ifdef CONFIG_FB_MIPI_DSIM
	struct clk *sclk_pll;

	s5p_dsim_set_platdata(&dsim_platform_data);
	s5p_device_mipi_dsim.dev.parent = &exynos5_device_pd[PD_DISP1].dev;
#endif
	s5p_device_fimd1.dev.parent = &exynos5_device_pd[PD_DISP1].dev;

	dev_set_name(&s5p_device_fimd1.dev, "s3cfb.1");
	clk_add_alias("lcd", "exynos5-fb.1", "lcd", &s5p_device_fimd1.dev);
	clk_add_alias("sclk_fimd", "exynos5-fb.1", "sclk_fimd",
			&s5p_device_fimd1.dev);
	s5p_fb_setname(1, "exynos5-fb");

	s5p_fimd1_set_platdata(&smdk5250_lcd1_pdata);

	samsung_bl_set(&smdk5250_bl_gpio_info, &smdk5250_bl_data);

	platform_add_devices(smdk5250_display_devices,
			ARRAY_SIZE(smdk5250_display_devices));

#ifdef CONFIG_FB_MIPI_DSIM
	sclk_pll = clk_get(&s5p_device_fimd1.dev, "fout_vpll");
	clk_set_rate(sclk_pll, 126000000);
	clk_put(sclk_pll);

	exynos4_fimd_setup_clock(&s5p_device_fimd1.dev,
			"sclk_fimd", "sclk_vpll", 126000000);
#endif
}
