/* linux/arch/arm/mach-exynos/board-smdk5250-input.c
 *
 * Copyright (c) 2012 Samsung Electronics Co., Ltd.
 *		http://www.samsung.com
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
*/

#include <linux/gpio.h>
#include <linux/gpio_keys.h>
#include <linux/i2c.h>
#include <linux/input.h>
#include <linux/delay.h>

#include <plat/gpio-cfg.h>
#include <plat/devs.h>
#include <plat/iic.h>

#include <mach/irqs.h>
#include <mach/board_rev.h>

#include "board-smdk5250.h"

struct gpio_keys_button smdk5250_button[] = {
	{
		.code = KEY_VOLUMEDOWN,
		.gpio = EXYNOS5_GPX1(6),
		.active_low = 1,
		.wakeup = 0,
	},
	{
		.code = KEY_VOLUMEUP,
		.gpio = EXYNOS5_GPX1(7),
		.active_low = 1,
		.wakeup = 0,
	},
	{
		.code = KEY_POWER,
		.gpio = EXYNOS5_GPX1(3),
		.active_low = 1,
		.wakeup = 1,
	},
};

struct gpio_keys_platform_data smdk5250_gpiokeys_platform_data = {
	smdk5250_button,
	ARRAY_SIZE(smdk5250_button),
};

static struct platform_device smdk5250_gpio_keys = {
	.name	= "gpio-keys",
	.dev	= {
		.platform_data = &smdk5250_gpiokeys_platform_data,
	},
};

static struct platform_device *smdk5250_input_devices[] __initdata = {
	&smdk5250_gpio_keys,
};

static void exynos5_smdk5250_touch_init(void)
{
	s3c_gpio_cfgpin(EXYNOS5_GPX1(3), S3C_GPIO_SFN(0xF));
	s3c_gpio_setpull(EXYNOS5_GPX1(3), S3C_GPIO_PULL_UP);
	s3c_gpio_cfgpin(EXYNOS5_GPX1(6), S3C_GPIO_SFN(0xF));
	s3c_gpio_setpull(EXYNOS5_GPX1(6), S3C_GPIO_PULL_UP);
	s3c_gpio_cfgpin(EXYNOS5_GPX1(7), S3C_GPIO_SFN(0xF));
	s3c_gpio_setpull(EXYNOS5_GPX1(7), S3C_GPIO_PULL_UP);

}

void __init exynos5_smdk5250_input_init(void)
{
	exynos5_smdk5250_touch_init();

	platform_add_devices(smdk5250_input_devices, ARRAY_SIZE(smdk5250_input_devices));

	s3c_gpio_setpull(EXYNOS5_GPX1(3), S3C_GPIO_PULL_UP);
}
