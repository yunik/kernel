/* linux/arch/arm/mach-exynos/mach-smdk5250.c
 *
 * Copyright (c) 2011 Samsung Electronics Co., Ltd.
 *		http://www.samsung.com
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
*/

#include <linux/platform_device.h>
#include <linux/serial_core.h>
#include <linux/clk.h>
#include <linux/gpio.h>
#include <linux/i2c.h>
#include <linux/regulator/machine.h>
#include <linux/regulator/fixed.h>
#include <linux/memblock.h>
#include <linux/rfkill-gpio.h>
#include <linux/delay.h>
#include <linux/notifier.h>
#include <linux/reboot.h>
#include <linux/huins_log.h>

#include <asm/mach/arch.h>
#include <asm/io.h>
#include <asm/mach-types.h>

#include <media/exynos_gscaler.h>
#include <media/exynos_flite.h>
#include <plat/gpio-cfg.h>
#include <plat/adc.h>
#include <plat/regs-adc.h>
#include <plat/regs-serial.h>
#include <plat/exynos5.h>
#include <plat/cpu.h>
#include <plat/clock.h>
#include <plat/hwmon.h>
#include <plat/devs.h>
#include <plat/regs-srom.h>
#include <plat/iic.h>
#include <plat/pd.h>
#include <plat/s5p-mfc.h>
#include <plat/fimg2d.h>
#include <plat/tv-core.h>
#include <plat/irqs.h>

#include <mach/map.h>
#include <mach/exynos-ion.h>
#include <mach/sysmmu.h>
#include <mach/ppmu.h>
#include <mach/dev.h>
#include <mach/regs-pmu.h>
#include <mach/regs-clock.h>
#include <mach/regs-pmu5.h>
#include <mach/board_rev.h>
#ifdef CONFIG_EXYNOS_CONTENT_PATH_PROTECTION
#include <mach/secmem.h>
#endif
#ifdef CONFIG_VIDEO_JPEG_V2X
#include <plat/jpeg.h>
#endif
#ifdef CONFIG_EXYNOS_C2C
#include <mach/c2c.h>
#endif
#ifdef CONFIG_EXYNOS_HSI
#include <mach/hsi.h>
#endif
#ifdef CONFIG_VIDEO_EXYNOS_TV
#include <plat/tvout.h>
#endif

#ifdef CONFIG_GPS_POWER
#include <mach/gcontrol.h>
#endif

#include <plat/media.h>

#include "board-smdk5250.h"

#define REG_INFORM4            (S5P_INFORM4)

/* Following are default values for UCON, ULCON and UFCON UART registers */
#define ACHRO5250_UCON_DEFAULT	(S3C2410_UCON_TXILEVEL |	\
				 S3C2410_UCON_RXILEVEL |	\
				 S3C2410_UCON_TXIRQMODE |	\
				 S3C2410_UCON_RXIRQMODE |	\
				 S3C2410_UCON_RXFIFO_TOI |	\
				 S3C2443_UCON_RXERR_IRQEN)

#define ACHRO5250_ULCON_DEFAULT	S3C2410_LCON_CS8

#define ACHRO5250_UFCON_DEFAULT	(S3C2410_UFCON_FIFOMODE |	\
				 S5PV210_UFCON_TXTRIG4 |	\
				 S5PV210_UFCON_RXTRIG4)

#if CONFIG_INV_SENSORS
#include <linux/mpu.h>
//gyro + accel
static struct mpu_platform_data mpu6050_data = {
	.int_config = 0x10,
	.level_shifter = 0,
	.orientation = { 1, 0, 0,
					0, -1, 0,
					0, 0, -1 },
};
//compass
static struct ext_slave_platform_data inv_mpu_ak8975_data = {
	.address	= 0x0C,
	.adapt_num	= 5,
	.bus		= EXT_SLAVE_BUS_PRIMARY,
	.orientation = { 1, 0, 0,
					0, -1, 0,
					0, 0, 1 },
};

// pressure
static struct ext_slave_platform_data inv_mpu_bmp180_data = {
	.address	= 0x77,
	.adapt_num	= 5,
	.bus		= EXT_SLAVE_BUS_PRIMARY,
};
#endif

static struct s3c2410_uartcfg achro5250_uartcfgs[] __initdata = {
	[0] = {
		.hwport		= 0,
		.flags		= 0,
		.ucon		= ACHRO5250_UCON_DEFAULT,
		.ulcon		= ACHRO5250_ULCON_DEFAULT,
		.ufcon		= ACHRO5250_UFCON_DEFAULT,
	},
	[1] = {
		.hwport		= 1,
		.flags		= 0,
		.ucon		= ACHRO5250_UCON_DEFAULT,
		.ulcon		= ACHRO5250_ULCON_DEFAULT,
		.ufcon		= ACHRO5250_UFCON_DEFAULT,
	},
	[2] = {
		.hwport		= 2,
		.flags		= 0,
		.ucon		= ACHRO5250_UCON_DEFAULT,
		.ulcon		= ACHRO5250_ULCON_DEFAULT,
		.ufcon		= ACHRO5250_UFCON_DEFAULT,
	},
	[3] = {
		.hwport		= 3,
		.flags		= 0,
		.ucon		= ACHRO5250_UCON_DEFAULT,
		.ulcon		= ACHRO5250_ULCON_DEFAULT,
		.ufcon		= ACHRO5250_UFCON_DEFAULT,
	},
};

#ifdef CONFIG_EXYNOS_MEDIA_DEVICE
struct platform_device exynos_device_md0 = {
	.name = "exynos-mdev",
	.id = 0,
};

struct platform_device exynos_device_md1 = {
	.name = "exynos-mdev",
	.id = 1,
};

struct platform_device exynos_device_md2 = {
	.name = "exynos-mdev",
	.id = 2,
};
#endif

#ifdef CONFIG_EXYNOS_C2C
struct exynos_c2c_platdata smdk5250_c2c_pdata = {
	.setup_gpio	= NULL,
	.shdmem_addr	= C2C_SHAREDMEM_BASE,
	.shdmem_size	= C2C_MEMSIZE_64,
	.ap_sscm_addr	= NULL,
	.cp_sscm_addr	= NULL,
	.rx_width	= C2C_BUSWIDTH_16,
	.tx_width	= C2C_BUSWIDTH_16,
	.clk_opp100	= 400,
	.clk_opp50	= 200,
	.clk_opp25	= 100,
	.default_opp_mode	= C2C_OPP50,
	.get_c2c_state	= NULL,
	.c2c_sysreg	= S5P_VA_CMU + 0x6000,
};
#endif

#ifdef CONFIG_EXYNOS_HSI
struct exynos_hsi_platdata smdk5250_hsi_pdata = {
	.setup_gpio	= NULL,
};
#endif

#ifdef CONFIG_VIDEO_FIMG2D
static struct fimg2d_platdata fimg2d_data __initdata = {
	.hw_ver		= 0x42,
	.gate_clkname	= "fimg2d",
};
#endif

//#ifdef CONFIG_RFKILL
/* Bluetooth rfkill gpio platform data */
struct rfkill_gpio_platform_data achro5250_bt_pdata = {
	.reset_gpio	= EXYNOS5_GPX2(7),
	.shutdown_gpio	= -1,
	.type		= RFKILL_TYPE_BLUETOOTH,
	.name		= "achro5250-bt",
};

/* Bluetooth Platform device */
static struct platform_device achro5250_device_bluetooth = {
	.name		= "rfkill_gpio",
	.id		= -1,
	.dev		= {
		.platform_data	= &achro5250_bt_pdata,
	},
};

static void __init achro5250_bt_setup(void)
{
	gpio_request(EXYNOS5_GPA0(0), "GPIO BT_UART");
	/* 4 UART Pins configuration */
	s3c_gpio_cfgrange_nopull(EXYNOS5_GPA0(0), 4, S3C_GPIO_SFN(2));
	/* Setup BT Reset, this gpio will be requesed by rfkill-gpio */
	s3c_gpio_cfgpin(EXYNOS5_GPX2(7), S3C_GPIO_OUTPUT);
	s3c_gpio_setpull(EXYNOS5_GPX2(7), S3C_GPIO_PULL_NONE);
}
//#endif

#ifdef CONFIG_SMSC911X
#include <linux/smsc911x.h>
static struct resource achro5250_smsc911x_resources[] = {
	[0] = {
		.start = EXYNOS4_PA_SROM_BANK(1),
		.end = EXYNOS4_PA_SROM_BANK(1) + SZ_64K - 1,
		.flags = IORESOURCE_MEM,
	},
	[1] = {
		.start = IRQ_EINT(0),
		.end = IRQ_EINT(0),
		.flags = IORESOURCE_IRQ | IORESOURCE_IRQ_LOWLEVEL,
	},
};

static struct smsc911x_platform_config smsc911x_config = {
	.irq_polarity = SMSC911X_IRQ_POLARITY_ACTIVE_LOW,
	.irq_type = SMSC911X_IRQ_TYPE_PUSH_PULL,
	.flags = SMSC911X_USE_16BIT | SMSC911X_FORCE_INTERNAL_PHY,
	.phy_interface = PHY_INTERFACE_MODE_MII,
};

static struct platform_device achro5250_device_smsc911x = {
	.name = "smsc911x",
	.id = -1,
	.num_resources = ARRAY_SIZE(achro5250_smsc911x_resources),
	.resource = achro5250_smsc911x_resources,
	.dev = {
		.platform_data = &smsc911x_config,
	},
};

static void achro5250_smsc911x_cfg_interface(void) {
	uint32_t temp;
	int i;

	// LAN9115 nRESET = 1
	s3c_gpio_cfgpin(EXYNOS5_GPD1(3), S3C_GPIO_OUTPUT);
	s3c_gpio_setpull(EXYNOS5_GPD1(3), S3C_GPIO_PULL_UP);
	gpio_set_value(EXYNOS5_GPD1(3), 1);

	// SROM_CSn[0..3], EBI_OEn, EBI_WEn
	for(i = 0; i < 6; i++) {
		s3c_gpio_cfgpin(EXYNOS5_GPY0(i), S3C_GPIO_SFN(0x2));
		s3c_gpio_setpull(EXYNOS5_GPY0(i), S3C_GPIO_PULL_NONE);
	}

	// EBI_BEn[0..1], SROM_WAITn, EBI_DATA_RDn
	for(i = 0; i < 4; i++) {
		s3c_gpio_cfgpin(EXYNOS5_GPY1(i), S3C_GPIO_SFN(0x2));
		s3c_gpio_setpull(EXYNOS5_GPY1(i), S3C_GPIO_PULL_NONE);
	}

	// EBI_ADDR[0..15], EBI_DATA[0..15]
	for(i = 0; i < 8; i++) {
		s3c_gpio_cfgpin(EXYNOS5_GPY3(i), S3C_GPIO_SFN(0x2));
		s3c_gpio_setpull(EXYNOS5_GPY3(i), S3C_GPIO_PULL_NONE);
		s3c_gpio_cfgpin(EXYNOS5_GPY4(i), S3C_GPIO_SFN(0x2));
		s3c_gpio_setpull(EXYNOS5_GPY4(i), S3C_GPIO_PULL_NONE);
		s3c_gpio_cfgpin(EXYNOS5_GPY5(i), S3C_GPIO_SFN(0x2));
		s3c_gpio_setpull(EXYNOS5_GPY5(i), S3C_GPIO_PULL_NONE);
		s3c_gpio_cfgpin(EXYNOS5_GPY6(i), S3C_GPIO_SFN(0x2));
		s3c_gpio_setpull(EXYNOS5_GPY6(i), S3C_GPIO_PULL_NONE);
	}

	// BANK1 : SMSC9115 Settings
	temp = __raw_readl(S5P_SROM_BW) &
		~(S5P_SROM_BW__CS_MASK << S5P_SROM_BW__NCS1__SHIFT);

	temp |= (
		(1 << S5P_SROM_BW__DATAWIDTH__SHIFT)
		| (0 << S5P_SROM_BW__WAITENABLE__SHIFT)
		| (1 << S5P_SROM_BW__BYTEENABLE__SHIFT)
		| (1 << S5P_SROM_BW__ADDRMODE__SHIFT)
		) << S5P_SROM_BW__NCS1__SHIFT;
	__raw_writel(temp, S5P_SROM_BW);

	__raw_writel(
            (0x0 << S5P_SROM_BCX__PMC__SHIFT)   |
            (0x6 << S5P_SROM_BCX__TACP__SHIFT)  |
            (0x4 << S5P_SROM_BCX__TCAH__SHIFT)  |
            (0x1 << S5P_SROM_BCX__TCOH__SHIFT)  |
            (0xe << S5P_SROM_BCX__TACC__SHIFT)  |
            (0x4 << S5P_SROM_BCX__TCOS__SHIFT)  |
            (0x0 << S5P_SROM_BCX__TACS__SHIFT),
            S5P_SROM_BC1);

	// BANK2 : Achro-FPGA Settings
	temp = __raw_readl(S5P_SROM_BW) &
		~(S5P_SROM_BW__CS_MASK << S5P_SROM_BW__NCS3__SHIFT);

	temp |= (
		(0 << S5P_SROM_BW__DATAWIDTH__SHIFT)
		| (0 << S5P_SROM_BW__WAITENABLE__SHIFT)
		| (1 << S5P_SROM_BW__BYTEENABLE__SHIFT)
		| (1 << S5P_SROM_BW__ADDRMODE__SHIFT)
		) << S5P_SROM_BW__NCS3__SHIFT;
	__raw_writel(temp, S5P_SROM_BW);

	__raw_writel(
            (0x0 << S5P_SROM_BCX__PMC__SHIFT)   |
            (0x6 << S5P_SROM_BCX__TACP__SHIFT)  |
            (0x4 << S5P_SROM_BCX__TCAH__SHIFT)  |
            (0x1 << S5P_SROM_BCX__TCOH__SHIFT)  |
            (0xd << S5P_SROM_BCX__TACC__SHIFT)  |
            (0x4 << S5P_SROM_BCX__TCOS__SHIFT)  |
            (0x0 << S5P_SROM_BCX__TACS__SHIFT),
            S5P_SROM_BC3);
}
#endif


static struct i2c_board_info i2c_devs2[] __initdata = {
#ifdef CONFIG_VIDEO_EXYNOS_TV
	{
		I2C_BOARD_INFO("exynos_hdcp", (0x74 >> 1)),
	},
#endif
};

static struct i2c_board_info i2c_devs3[] __initdata = {
	{
		I2C_BOARD_INFO("ak4678", 0x12),
	},
#if defined(CONFIG_SND_SOC_ALC5631)
	{
		I2C_BOARD_INFO("alc5631", 0x1a),
	},
#endif
};

struct s3c2410_platform_i2c i2c_data3 __initdata = {
	.bus_num	= 3,
	.flags		= 0,
	.slave_addr	= 0x10,
	.frequency	= 200*1000,
	.sda_delay	= 100,
};

static struct i2c_board_info i2c_devs4[] __initdata = {
#ifdef CONFIG_INV_SENSORS
		{
				I2C_BOARD_INFO("ak8975", 0x0C),
				.irq = IRQ_EINT(5),
				.platform_data = &inv_mpu_ak8975_data,
		},
#endif
};

#ifdef CONFIG_CHARGER_MAX8903
#include <linux/power/max8903_charger.h>
static void max8903_gpio_init(void) {
	// CEN
	s3c_gpio_cfgpin(EXYNOS5_GPZ(5), S3C_GPIO_OUTPUT);
	s3c_gpio_setpull(EXYNOS5_GPZ(5), S3C_GPIO_PULL_UP);
	// DOK
	s3c_gpio_cfgpin(EXYNOS5_GPX3(0), S3C_GPIO_SFN(0xF));
	s3c_gpio_setpull(EXYNOS5_GPX3(0), S3C_GPIO_PULL_UP);
	// UOK
	s3c_gpio_cfgpin(EXYNOS5_GPX2(6), S3C_GPIO_SFN(0xF));
	s3c_gpio_setpull(EXYNOS5_GPX2(6), S3C_GPIO_PULL_UP);
	// CHG
	s3c_gpio_cfgpin(EXYNOS5_GPX3(1), S3C_GPIO_SFN(0xF));
	s3c_gpio_setpull(EXYNOS5_GPX3(1), S3C_GPIO_PULL_UP);
	// FLT
	s3c_gpio_cfgpin(EXYNOS5_GPX1(4), S3C_GPIO_SFN(0xF));
	s3c_gpio_setpull(EXYNOS5_GPX1(4), S3C_GPIO_PULL_UP);
}

static struct max8903_pdata max8903_pdata = {
//	.cen = EXYNOS5_GPZ(5),
	.dok = EXYNOS5_GPX3(0),
	.uok = EXYNOS5_GPX2(6),
	.chg = EXYNOS5_GPX3(1),
	.flt = EXYNOS5_GPX1(4),
	.dc_valid = true,
	.usb_valid = true,
};

static struct platform_device max8903_device = {
	.name = "max8903-charger",
	.id = -1,
	.dev = {
		.platform_data = &max8903_pdata,
	},
};
#endif

#ifdef CONFIG_BATTERY_MAX17040
#include <linux/max17040_battery.h>

static int max17040_battery_online(void) {
	// assume battery connected
	return 1;
}

static int max17040_charger_online(void) {
	if (!gpio_get_value(EXYNOS5_GPX3(0)) || !gpio_get_value(EXYNOS5_GPX2(6))) {
		return 1;
	} else {
		return 0;
	}
}

static int max17040_charger_status(void) {
	return gpio_get_value(EXYNOS5_GPX3(1)) ? 0 : 1;
}

struct max17040_platform_data max17040_pdata = {
	.battery_online = max17040_battery_online,
	.charger_online = max17040_charger_online,
	.charger_enable = max17040_charger_status,
};
#endif


static struct i2c_board_info i2c_devs5[] __initdata = {
#ifdef CONFIG_INV_SENSORS
        {
                I2C_BOARD_INFO("mpu6050", 0x68),
                .irq = IRQ_EINT(10),
                .platform_data = &mpu6050_data,
        },
#endif
		{
				I2C_BOARD_INFO("bh1780", 0x29),
		},
#ifdef CONFIG_BATTERY_MAX17040
		{
				I2C_BOARD_INFO("max17040", 0x36),
				.platform_data = &max17040_pdata,
		},
#endif
#ifdef CONFIG_MPU_SENSORS_BMA085
		{
				I2C_BOARD_INFO("bma085", 0x77),
				.platform_data = &inv_mpu_bmp180_data,
		},
#endif
};

static struct i2c_board_info i2c_devs7[] __initdata = {
#ifdef CONFIG_TOUCHSCREEN_EP0700MLH0
	{
		I2C_BOARD_INFO("ep0700mlh0", 0x38),
	},
#endif
};

#ifdef CONFIG_S3C_DEV_HWMON
static struct s3c_hwmon_pdata smdk5250_hwmon_pdata __initdata = {
	/* Reference voltage (1.2V) */
	.in[0] = &(struct s3c_hwmon_chcfg) {
		.name		= "smdk:reference-voltage",
		.mult		= 3300,
		.div		= 4096,
	},
};
#endif

static int exynos5_notifier_call(struct notifier_block *this,
		unsigned long code, void *_cmd)
{
	int mode = 0;

	switch(code) {
	case SYS_RESTART:
		if(_cmd) {
			LOG("Requested to RESTART, CMD = %s", (char *)_cmd);
		} else {
			LOG("Requested to RESTART, CMD = NULL");
		}
		break;
	case SYS_HALT:
		LOG("Requested to HALT");
		break;
	case SYS_POWER_OFF:
		LOG("Requested to POWER OFF");
		break;
	}

	if ((code == SYS_RESTART) && _cmd)
		if (!strcmp((char *)_cmd, "recovery"))
			mode = 0xf;

	__raw_writel(mode, REG_INFORM4);

	return NOTIFY_DONE;
}

static struct notifier_block exynos5_reboot_notifier = {
	.notifier_call = exynos5_notifier_call,
};

#ifdef CONFIG_GPS_POWER
#define CUSTOM_GPIO_GPS_POWER_EN EXYNOS5_GPX0(7)
#define CUSTOM_GPIO_GPS_RESET EXYNOS5_GPB1(2)
//#define CUSTOM_GPIO_GPS_RESET_TMP EXYNOS5_GPA1(5)

struct csr_platform_data csr_platdata = {
	.power = CUSTOM_GPIO_GPS_POWER_EN,
	.reset = CUSTOM_GPIO_GPS_RESET,
};

static struct platform_device csr_gps = {
	.name           = "csrgps",
	.id		= -1,
	.dev = {
	.platform_data = &csr_platdata,
	},
};

static struct platform_device csr_gps_reset = {
	.name           = "csrgpsreset",
	.id				= -1,
	.dev = {
		.platform_data = &csr_platdata,
	},
};
#endif

static struct platform_device *achro5250_devices[] __initdata = {
	&s3c_device_wdt,
	&s3c_device_i2c2,
	&s3c_device_i2c3,
	&s3c_device_i2c4,
	&s3c_device_i2c5,
	&s3c_device_i2c7,

	&exynos_device_i2s0,
	&samsung_asoc_dma,
	&samsung_asoc_idma,

#if defined(CONFIG_CHARGER_MAX8903)
	&max8903_device,
#endif

#if defined(CONFIG_VIDEO_SAMSUNG_S5P_MFC)
	&s5p_device_mfc,
#endif
#ifdef CONFIG_VIDEO_JPEG_V2X
	&s5p_device_jpeg,
#endif
#ifdef CONFIG_ION_EXYNOS
	&exynos_device_ion,
#endif
#ifdef CONFIG_VIDEO_FIMG2D
	&s5p_device_fimg2d,
#endif
#ifdef CONFIG_EXYNOS_MEDIA_DEVICE
	&exynos_device_md0,
	&exynos_device_md1,
	&exynos_device_md2,
#endif
#ifdef CONFIG_VIDEO_EXYNOS_GSCALER
	&exynos5_device_gsc0,
	&exynos5_device_gsc1,
	&exynos5_device_gsc2,
	&exynos5_device_gsc3,
#endif
#ifdef CONFIG_VIDEO_EXYNOS_ROTATOR
	&exynos_device_rotator,
#endif
	&s3c_device_rtc,
#ifdef CONFIG_VIDEO_EXYNOS_TV
#ifdef CONFIG_VIDEO_EXYNOS_HDMI
	&s5p_device_hdmi,
#endif
#ifdef CONFIG_VIDEO_EXYNOS_HDMIPHY
	&s5p_device_i2c_hdmiphy,
#endif
#ifdef CONFIG_VIDEO_EXYNOS_MIXER
	&s5p_device_mixer,
#endif
#ifdef CONFIG_VIDEO_EXYNOS_HDMI_CEC
	&s5p_device_cec,
#endif
#endif
#ifdef CONFIG_S5P_DEV_ACE
	&s5p_device_ace,
#endif
#ifdef CONFIG_EXYNOS_C2C
	&exynos_device_c2c,
#endif
#ifdef CONFIG_EXYNOS_HSI
	&exynos_device_hsi,
#endif
	&exynos5_device_ahci,

//#ifdef CONFIG_RFKILL
	&achro5250_device_bluetooth,
//#endif
#ifdef CONFIG_GPS_POWER
	&csr_gps,
	&csr_gps_reset,
#endif

#ifdef CONFIG_SMSC911X
	&achro5250_device_smsc911x,
#endif
};

#ifdef CONFIG_VIDEO_EXYNOS_HDMI_CEC
static struct s5p_platform_cec hdmi_cec_data __initdata = {

};
#endif

#if defined(CONFIG_CMA)
static void __init exynos_reserve_mem(void)
{
	static struct cma_region regions[] = {
		{
			.name = "ion",
			.size = 30 * SZ_1M,
			.start = 0
		},
#ifdef CONFIG_VIDEO_SAMSUNG_MEMSIZE_GSC0
		{
			.name = "gsc0",
			.size = CONFIG_VIDEO_SAMSUNG_MEMSIZE_GSC0 * SZ_1K,
			.start = 0
		},
#endif
#ifdef CONFIG_VIDEO_SAMSUNG_MEMSIZE_GSC1
		{
			.name = "gsc1",
			.size = CONFIG_VIDEO_SAMSUNG_MEMSIZE_GSC1 * SZ_1K,
			.start = 0
		},
#endif
#ifdef CONFIG_VIDEO_SAMSUNG_MEMSIZE_GSC2
		{
			.name = "gsc2",
			.size = CONFIG_VIDEO_SAMSUNG_MEMSIZE_GSC2 * SZ_1K,
			.start = 0
		},
#endif
#ifdef CONFIG_VIDEO_SAMSUNG_MEMSIZE_GSC3
		{
			.name = "gsc3",
			.size = CONFIG_VIDEO_SAMSUNG_MEMSIZE_GSC3 * SZ_1K,
			.start = 0
		},
#endif
#ifdef CONFIG_VIDEO_SAMSUNG_MEMSIZE_FLITE0
		{
			.name = "flite0",
			.size = CONFIG_VIDEO_SAMSUNG_MEMSIZE_FLITE0 * SZ_1K,
			.start = 0
		},
#endif
#ifdef CONFIG_VIDEO_SAMSUNG_MEMSIZE_FLITE1
		{
			.name = "flite1",
			.size = CONFIG_VIDEO_SAMSUNG_MEMSIZE_FLITE1 * SZ_1K,
			.start = 0
		},
#endif
#ifdef CONFIG_VIDEO_SAMSUNG_MEMSIZE_FIMD
		{
			.name = "fimd",
			.size = CONFIG_VIDEO_SAMSUNG_MEMSIZE_FIMD * SZ_1K,
			.start = 0
		},
#endif
#ifdef CONFIG_AUDIO_SAMSUNG_MEMSIZE_SRP
		{
			.name = "srp",
			.size = CONFIG_AUDIO_SAMSUNG_MEMSIZE_SRP * SZ_1K,
			.start = 0,
		},
#endif
#ifdef CONFIG_VIDEO_SAMSUNG_S5P_MFC
		{
			.name		= "fw",
			.size		= 2 << 20,
			{ .alignment	= 128 << 10 },
			.start		= 0x44000000,
		},
		{
			.name		= "b1",
			.size		= 64 << 20,
			.start		= 0x45000000,
		},
#endif
#ifdef CONFIG_VIDEO_SAMSUNG_MEMSIZE_TV
		{
			.name = "tv",
			.size = CONFIG_VIDEO_SAMSUNG_MEMSIZE_TV * SZ_1K,
			.start = 0
		},
#endif
#ifdef CONFIG_VIDEO_SAMSUNG_MEMSIZE_ROT
		{
			.name = "rot",
			.size = CONFIG_VIDEO_SAMSUNG_MEMSIZE_ROT * SZ_1K,
			.start = 0,
		},
#endif
#ifdef CONFIG_EXYNOS_CONTENT_PATH_PROTECTION
#ifdef CONFIG_ION_EXYNOS_DRM_MFC_SH
		{
			.name = "drm_mfc_sh",
			.size = SZ_1M,
		},
#endif
#endif
		{
			.size = 0
		},
	};
#ifdef CONFIG_EXYNOS_CONTENT_PATH_PROTECTION
	static struct cma_region regions_secure[] = {
#ifdef CONFIG_ION_EXYNOS_DRM_VIDEO
		{
			.name = "drm_video",
			.size = (
#ifdef CONFIG_ION_EXYNOS_DRM_MEMSIZE_FIMD_VIDEO
				CONFIG_ION_EXYNOS_DRM_MEMSIZE_FIMD_VIDEO +
#endif
#ifdef CONFIG_ION_EXYNOS_DRM_MEMSIZE_GSC
				CONFIG_ION_EXYNOS_DRM_MEMSIZE_GSC +
#endif
#ifdef CONFIG_ION_EXYNOS_DRM_MEMSIZE_MFC_SECURE
				CONFIG_ION_EXYNOS_DRM_MEMSIZE_MFC_SECURE +
#endif
				0) * SZ_1K,
		},
#endif
#ifdef CONFIG_ION_EXYNOS_DRM_MFC_FW
		{
			.name = "drm_mfc_fw",
			.size = SZ_1M,
		},
#endif
#ifdef CONFIG_ION_EXYNOS_DRM_SECTBL
		{
			.name = "drm_sectbl",
			.size = SZ_1M,
		},
#endif
		{
			.size = 0
		},
	};
#else /* !CONFIG_EXYNOS_CONTENT_PATH_PROTECTION */
	struct cma_region *regions_secure = NULL;
#endif /* CONFIG_EXYNOS_CONTENT_PATH_PROTECTION */
	static const char map[] __initconst =
#ifdef CONFIG_EXYNOS_C2C
		"samsung-c2c=c2c_shdmem;"
#endif
		"s3cfb.0=fimd;exynos5-fb.1=fimd;"
		"samsung-rp=srp;"
		"exynos-gsc.0=gsc0;exynos-gsc.1=gsc1;exynos-gsc.2=gsc2;exynos-gsc.3=gsc3;"
#ifdef CONFIG_EXYNOS_CONTENT_PATH_PROTECTION
		"ion-exynos/mfc_sh=drm_mfc_sh;"
		"ion-exynos/video=drm_video;"
		"ion-exynos/mfc_fw=drm_mfc_fw;"
		"ion-exynos/sectbl=drm_sectbl;"
		"s5p-smem/mfc_sh=drm_mfc_sh;"
		"s5p-smem/video=drm_video;"
		"s5p-smem/mfc_fw=drm_mfc_fw;"
		"s5p-smem/sectbl=drm_sectbl;"
#endif
		"ion-exynos=ion,gsc0,gsc1,gsc2,gsc3,flite0,flite1,fimd,fw,b1,rot;"
		"exynos-rot=rot;"
		"s5p-mfc-v6/f=fw;"
		"s5p-mfc-v6/a=b1;"
		"s5p-mixer=tv;"
;
	s5p_cma_region_reserve(regions, regions_secure, 0, map);
}
#else /* !CONFIG_CMA*/
static inline void exynos_reserve_mem(void)
{
}
#endif

#if defined(CONFIG_VIDEO_SAMSUNG_S5P_MFC)
static struct s5p_mfc_platdata smdk5250_mfc_pd = {
	.clock_rate = 333000000,
	.lock_thrd_w = 1920,
	.lock_thrd_h = 1080,
	.lock_freq_mem = 100,
	.lock_freq_bus = 160,
};
#endif

static void __init achro5250_map_io(void)
{
	clk_xxti.rate = 24000000;
	s5p_init_io(NULL, 0, S5P_VA_CHIPID);
	s3c24xx_init_clocks(24000000);
	s3c24xx_init_uarts(achro5250_uartcfgs, ARRAY_SIZE(achro5250_uartcfgs));
	exynos_reserve_mem();
}

#ifdef CONFIG_EXYNOS_DEV_SYSMMU
static void __init exynos_sysmmu_init(void)
{
#ifdef CONFIG_VIDEO_JPEG_V2X
	platform_set_sysmmu(&SYSMMU_PLATDEV(jpeg).dev, &s5p_device_jpeg.dev);
#endif
#if defined(CONFIG_VIDEO_SAMSUNG_S5P_MFC)
	platform_set_sysmmu(&SYSMMU_PLATDEV(mfc_lr).dev, &s5p_device_mfc.dev);
#endif
#if defined(CONFIG_VIDEO_EXYNOS_TV)
	platform_set_sysmmu(&SYSMMU_PLATDEV(tv).dev, &s5p_device_mixer.dev);
#endif
#ifdef CONFIG_VIDEO_EXYNOS_GSCALER
	platform_set_sysmmu(&SYSMMU_PLATDEV(gsc0).dev,
						&exynos5_device_gsc0.dev);
	platform_set_sysmmu(&SYSMMU_PLATDEV(gsc1).dev,
						&exynos5_device_gsc1.dev);
	platform_set_sysmmu(&SYSMMU_PLATDEV(gsc2).dev,
						&exynos5_device_gsc2.dev);
	platform_set_sysmmu(&SYSMMU_PLATDEV(gsc3).dev,
						&exynos5_device_gsc3.dev);
#endif
#ifdef CONFIG_VIDEO_EXYNOS_ROTATOR
	platform_set_sysmmu(&SYSMMU_PLATDEV(rot).dev,
						&exynos_device_rotator.dev);
#endif
#ifdef CONFIG_VIDEO_FIMG2D
	platform_set_sysmmu(&SYSMMU_PLATDEV(2d).dev, &s5p_device_fimg2d.dev);
#endif
}
#else /* !CONFIG_EXYNOS_DEV_SYSMMU */
static inline void exynos_sysmmu_init(void)
{
}
#endif

#define SMDK5250_REV_0_0_ADC_VALUE 0
#define SMDK5250_REV_0_2_ADC_VALUE 500

#define PMUREG_ISP_CONFIGURATION	(S5P_VA_PMU  + 0x4020)
#define PMUREG_ISP_STATUS		(S5P_VA_PMU  + 0x4024)

int samsung_board_rev;

static int get_samsung_board_rev(void)
{
	int ret = 0;
	int adc_val = 0;
	void __iomem *adc_regs;
	unsigned int timeout, con;

	writel(0x7, PMUREG_ISP_CONFIGURATION);
	timeout = 1000;
	while ((__raw_readl(PMUREG_ISP_STATUS) & 0x7) != 0x7) {
		if (timeout == 0)
			err("A5 power on failed1\n");
		timeout--;
		udelay(1);
		goto err_power;
	}
	__raw_writel(0x1, EXYNOS5_MTCADC_PHY_CONTROL);

	__raw_writel(0x00000031, EXYNOS5_CLKDIV_ISP0);
	__raw_writel(0x00000031, EXYNOS5_CLKDIV_ISP1);
	__raw_writel(0x00000001, EXYNOS5_CLKDIV_ISP2);

	__raw_writel(0xDFF000FF, EXYNOS5_CLKGATE_ISP0);
	__raw_writel(0x00003007, EXYNOS5_CLKGATE_ISP1);

	adc_regs = ioremap(EXYNOS5_PA_FIMC_IS_ADC, SZ_4K);
	if (unlikely(!adc_regs))
		goto err_power;

	/* SELMUX Channel 3 */
	writel(S5PV210_ADCCON_SELMUX(3), adc_regs + S5P_ADCMUX);

	con = readl(adc_regs + S3C2410_ADCCON);

	con &= ~S3C2410_ADCCON_MUXMASK;
	con &= ~S3C2410_ADCCON_STDBM;
	con &= ~S3C2410_ADCCON_STARTMASK;
	con |=  S3C2410_ADCCON_PRSCEN;

	/* ENABLE START */
	con |= S3C2410_ADCCON_ENABLE_START;
	writel(con, adc_regs + S3C2410_ADCCON);

	udelay (50);

	/* Read Data*/
	adc_val = readl(adc_regs + S3C2410_ADCDAT0) & 0xFFF;
	/* CLRINT */
	writel(0, adc_regs + S3C64XX_ADCCLRINT);

	iounmap(adc_regs);
err_power:
	ret = (adc_val < SMDK5250_REV_0_2_ADC_VALUE/2) ?
			SAMSUNG_BOARD_REV_0_0 : SAMSUNG_BOARD_REV_0_2;

	pr_info ("SMDK MAIN Board Rev 0.%d (ADC value:%d)\n", ret, adc_val);
	return ret;
}

static void __init achro5250_machine_init(void)
{
	int ret;
	samsung_board_rev = get_samsung_board_rev();
#ifdef CONFIG_SMSC911X
	achro5250_smsc911x_cfg_interface();
#endif
	exynos5_smdk5250_mmc_init();
	exynos5_smdk5250_power_init();
	exynos5_smdk5250_usb_init();
	exynos5_smdk5250_input_init();

#ifdef CONFIG_CHARGER_MAX8903
	max8903_gpio_init();
#endif

	ret = gpio_request(EXYNOS5_GPE0(1), "p5v0_en");
	if(ret) {
		LOG("request for p5v0_en (GPE0_1) failed");
	} else {
		gpio_direction_output(EXYNOS5_GPE0(1), 1);
		gpio_set_value(EXYNOS5_GPE0(1), 1);
	}

	ret = gpio_request(EXYNOS5_GPF1(3), "p1v2 en");
	if(ret) {
		printk(KERN_INFO "p1v2 en request failed\n");
	} else {
		gpio_direction_output(EXYNOS5_GPF1(3), 1);
		gpio_set_value(EXYNOS5_GPF1(3), 1);
	}

	s3c_gpio_cfgpin(EXYNOS5_GPX2(0), S3C_GPIO_INPUT);
	s3c_gpio_setpull(EXYNOS5_GPX2(0), S3C_GPIO_PULL_NONE);

	s3c_gpio_setpull(EXYNOS5_GPC2(2), S3C_GPIO_PULL_DOWN);
	s5p_gpio_set_drvstr(EXYNOS5_GPC2(2), S5P_GPIO_DRVSTR_LV4);

	s3c_i2c2_set_platdata(NULL);
	i2c_register_board_info(2, i2c_devs2, ARRAY_SIZE(i2c_devs2));

	s3c_i2c3_set_platdata(&i2c_data3);
	i2c_register_board_info(3, i2c_devs3, ARRAY_SIZE(i2c_devs3));

	s3c_i2c4_set_platdata(NULL);
	i2c_register_board_info(4, i2c_devs4, ARRAY_SIZE(i2c_devs4));

	s3c_i2c5_set_platdata(NULL);
	i2c_register_board_info(5, i2c_devs5, ARRAY_SIZE(i2c_devs5));

	s3c_i2c7_set_platdata(NULL);
	i2c_register_board_info(7, i2c_devs7, ARRAY_SIZE(i2c_devs7));

	ret = gpio_request(EXYNOS5_GPX2(1), "ep0700mlh0 reset");
	if (!ret) {
		gpio_direction_output(EXYNOS5_GPX2(1), 0);
		mdelay(10);
		gpio_direction_output(EXYNOS5_GPX2(1), 1);
		mdelay(10);
		gpio_free(EXYNOS5_GPX2(1));
	} else {
		printk(KERN_INFO "ep0700mlh0: request gpx2(1) failed\n");
	}

	s3c_gpio_cfgpin(EXYNOS5_GPX2(2), S3C_GPIO_SFN(0xF));
	s3c_gpio_setpull(EXYNOS5_GPX2(2), S3C_GPIO_PULL_UP);

#ifdef CONFIG_ION_EXYNOS
	exynos_ion_set_platdata();
#endif

	if (samsung_rev() >= EXYNOS5250_REV_1_0) {
		platform_device_register(&s3c_device_adc);
#ifdef CONFIG_S3C_DEV_HWMON
		platform_device_register(&s3c_device_hwmon);
#endif
	}

#ifdef CONFIG_S3C_DEV_HWMON
	if (samsung_rev() >= EXYNOS5250_REV_1_0)
		s3c_hwmon_set_platdata(&smdk5250_hwmon_pdata);
#endif

#if defined(CONFIG_VIDEO_SAMSUNG_S5P_MFC)
#if defined(CONFIG_EXYNOS_DEV_PD)
	s5p_device_mfc.dev.parent = &exynos5_device_pd[PD_MFC].dev;
#endif
	s5p_mfc_set_platdata(&smdk5250_mfc_pd);

	dev_set_name(&s5p_device_mfc.dev, "s3c-mfc");
	clk_add_alias("mfc", "s5p-mfc-v6", "mfc", &s5p_device_mfc.dev);
	s5p_mfc_setname(&s5p_device_mfc, "s5p-mfc-v6");
#endif

#ifdef CONFIG_VIDEO_FIMG2D
	s5p_fimg2d_set_platdata(&fimg2d_data);
#endif
	exynos_sysmmu_init();

	platform_add_devices(achro5250_devices, ARRAY_SIZE(achro5250_devices));

	exynos5_smdk5250_display_init();

#ifdef CONFIG_VIDEO_EXYNOS_GSCALER
#if defined(CONFIG_EXYNOS_DEV_PD)
	exynos5_device_gsc0.dev.parent = &exynos5_device_pd[PD_GSCL].dev;
	exynos5_device_gsc1.dev.parent = &exynos5_device_pd[PD_GSCL].dev;
	exynos5_device_gsc2.dev.parent = &exynos5_device_pd[PD_GSCL].dev;
	exynos5_device_gsc3.dev.parent = &exynos5_device_pd[PD_GSCL].dev;
#endif
#ifdef CONFIG_EXYNOS_CONTENT_PATH_PROTECTION
	secmem.parent = &exynos5_device_pd[PD_GSCL].dev;
#endif
	if (samsung_rev() >= EXYNOS5250_REV_1_0) {
		exynos5_gsc_set_pdev_name(0, "exynos5250-gsc");
		exynos5_gsc_set_pdev_name(1, "exynos5250-gsc");
		exynos5_gsc_set_pdev_name(2, "exynos5250-gsc");
		exynos5_gsc_set_pdev_name(3, "exynos5250-gsc");
	}

	s3c_set_platdata(&exynos_gsc0_default_data, sizeof(exynos_gsc0_default_data),
			&exynos5_device_gsc0);
	s3c_set_platdata(&exynos_gsc1_default_data, sizeof(exynos_gsc1_default_data),
			&exynos5_device_gsc1);
	s3c_set_platdata(&exynos_gsc2_default_data, sizeof(exynos_gsc2_default_data),
			&exynos5_device_gsc2);
	s3c_set_platdata(&exynos_gsc3_default_data, sizeof(exynos_gsc3_default_data),
			&exynos5_device_gsc3);
#endif
#ifdef CONFIG_EXYNOS_C2C
	exynos_c2c_set_platdata(&smdk5250_c2c_pdata);
#endif

#ifdef CONFIG_EXYNOS_HSI
	exynos_hsi_set_platdata(&smdk5250_hsi_pdata);
#endif

#ifdef CONFIG_VIDEO_JPEG_V2X
	exynos5_jpeg_setup_clock(&s5p_device_jpeg.dev, 150000000);
#endif

#ifdef CONFIG_SND_SAMSUNG_I2S
#if defined(CONFIG_EXYNOS_DEV_PD)
	exynos_device_i2s0.dev.parent = &exynos5_device_pd[PD_MAUDIO].dev;
#endif
#endif

#if defined(CONFIG_VIDEO_EXYNOS_TV) && defined(CONFIG_VIDEO_EXYNOS_HDMI)
	dev_set_name(&s5p_device_hdmi.dev, "exynos5-hdmi");
	clk_add_alias("hdmi", "s5p-hdmi", "hdmi", &s5p_device_hdmi.dev);
	clk_add_alias("hdmiphy", "s5p-hdmi", "hdmiphy", &s5p_device_hdmi.dev);

	s5p_tv_setup();

/* setup dependencies between TV devices */
	/* This will be added after power domain for exynos5 is developed */
	s5p_device_hdmi.dev.parent = &exynos5_device_pd[PD_DISP1].dev;
	s5p_device_mixer.dev.parent = &exynos5_device_pd[PD_DISP1].dev;

	s5p_i2c_hdmiphy_set_platdata(NULL);
#ifdef CONFIG_VIDEO_EXYNOS_HDMI_CEC
	s5p_hdmi_cec_set_platdata(&hdmi_cec_data);
#endif
#endif

//#ifdef CONFIG_RFKILL
	achro5250_bt_setup();
//#endif

	register_reboot_notifier(&exynos5_reboot_notifier);
}

#ifdef CONFIG_EXYNOS_C2C
static void __init exynos_c2c_reserve(void)
{
	static struct cma_region regions[] = {
		{
			.name = "c2c_shdmem",
			.size = 64 * SZ_1M,
			{ .alignment	= 64 * SZ_1M },
			.start = C2C_SHAREDMEM_BASE
		}, {
			.size = 0,
		}
	};

	s5p_cma_region_reserve(regions, NULL, 0, NULL);
}
#endif

static void __init achro5250_fixup(struct machine_desc *desc,
				struct tag *tags, char **cmdline,
				struct meminfo *mi)
{
	mi->bank[0].start = 0x40000000;
	mi->bank[0].size = 1024 * SZ_1M;

	mi->bank[1].start = 0x80000000;
	mi->bank[1].size = 1023 * SZ_1M;

	mi->nr_banks = 2;
}

MACHINE_START(ACHRO5250, "achro5250")
	.boot_params	= S5P_PA_SDRAM + 0x100,
	.init_irq	= exynos5_init_irq,
	.fixup		= achro5250_fixup,
	.map_io		= achro5250_map_io,
	.init_machine	= achro5250_machine_init,
	.timer		= &exynos4_timer,
#ifdef CONFIG_EXYNOS_C2C
	.reserve	= &exynos_c2c_reserve,
#endif
MACHINE_END
