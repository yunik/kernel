/*
 * huins_log.h
 *
 *  Created on: 2013. 1. 30.
 *      Author: hsjung
 */

#ifndef HUINS_LOG_H_
#define HUINS_LOG_H_

#define LOG(x,...) printk(KERN_INFO "[H][%s:%d] %s() - "x"\n", \
		__FILE__, __LINE__, __func__, ##__VA_ARGS__)

#endif /* HUINS_LOG_H_ */
